package com.sd.careapp.config;
/*
import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class SimpleAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest arg0, HttpServletResponse arg1,
                                        Authentication authentication) throws IOException, ServletException {

        boolean hasDoctorRole = false;
        boolean hasCaregiverRole = false;
        boolean hasPatientRole = false;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals("ROLE_DOCTOR")) {
                hasDoctorRole = true;
                break;
            } else if (grantedAuthority.getAuthority().equals("ROLE_CAREGIVER")) {
                hasCaregiverRole = true;
                break;
            }else if (grantedAuthority.getAuthority().equals("ROLE_PATIENT")) {
                hasPatientRole = true;
                break;
            }
        }

        if (hasDoctorRole) {
            redirectStrategy.sendRedirect(arg0, arg1, "/doctor");
        } else if (hasCaregiverRole) {
            redirectStrategy.sendRedirect(arg0, arg1, "/caregiver");
        } else if (hasPatientRole) {
            redirectStrategy.sendRedirect(arg0, arg1, "/patient");
        } else {
            throw new IllegalStateException();
        }
    }
}
*/