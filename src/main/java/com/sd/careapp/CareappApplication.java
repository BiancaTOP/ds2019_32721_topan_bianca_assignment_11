package com.sd.careapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
        }
)
public class CareappApplication {

    public static void main(String[] args) {
        SpringApplication.run(CareappApplication.class, args);
    }

}
