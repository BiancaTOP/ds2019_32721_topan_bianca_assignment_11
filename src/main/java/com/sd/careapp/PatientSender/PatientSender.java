package com.sd.careapp.PatientSender;


public interface PatientSender {

    void sendPatient(String username, String password, String name, String birthdate, String gender, String address);

}
