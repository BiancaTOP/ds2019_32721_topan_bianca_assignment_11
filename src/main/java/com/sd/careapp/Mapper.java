package com.sd.careapp;

import com.sd.careapp.entity.Caregiver;
import com.sd.careapp.entity.User;
import com.sd.careapp.repository.CaregiverRepository;
import com.sd.careapp.repository.UserRepository;
import com.sd.careapp.viewmodel.CaregiverViewModel;

import java.util.UUID;

public class Mapper {

    private UserRepository userRepository;

    public Mapper(CaregiverRepository caregiverRepository) {
        this.userRepository = userRepository;
    }

    public CaregiverViewModel convertToCaregiverViewModel(User entity) {
        var viewModel = new CaregiverViewModel();
        viewModel.setId(entity.getId());
        viewModel.setUsername(entity.getUsername());
        viewModel.setPassword(entity.getPassword());
        viewModel.setBirthdate(entity.getBirthdate());
        viewModel.setGender(entity.getGender());
        viewModel.setAddress(entity.getAddress());

        return viewModel;
    }

    public User convertToCaregiverEntity(CaregiverViewModel viewModel) {
        var entity = new User(viewModel.getId(), viewModel.getUsername(), viewModel.getPassword(), viewModel.getName(), viewModel.getBirthdate(), viewModel.getGender(), viewModel.getAddress());

        return entity;
    }

    /*
    public CaregiverViewModel caregiverViewModel(User entity) {
        var viewModel = new CaregiverViewModel();
        viewModel.setId(entity.getId());
        viewModel.setUsername(entity.getUsername());
        viewModel.setPassword(entity.getPassword());
        viewModel.setName(entity.getName());
        viewModel.setBirthdate(entity.getBirthdate());
        viewModel.setGender(entity.getGender());
        viewModel.setAddress(entity.getAddress());

        return viewModel;
    }


    public Notebook convertToNotebookEntity(NotebookViewModel viewModel) {
        var entity = new Notebook(viewModel.getId(), viewModel.getName());

        return entity;
    }
*/
}
