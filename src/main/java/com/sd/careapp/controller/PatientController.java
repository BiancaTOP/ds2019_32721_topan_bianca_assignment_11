package com.sd.careapp.controller;

import com.sd.careapp.PatientSender.PatientSender;
import com.sd.careapp.viewmodel.AddPatientViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;

@RestController
@RequestMapping(value = "/addpatient")
@CrossOrigin
public class PatientController {


    public PatientController(){}

  //  BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
    PatientSender patientSender;

    public PatientController(PatientSender patientSender) {
        this.patientSender = patientSender;
    }

    @PostMapping
    public void sendPatient(@RequestBody AddPatientViewModel addPatientViewModel,
                             BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            try {
                throw new ValidationException("Form has errors; Can not send feedback;");
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        }


        String username = addPatientViewModel.getUsername();
        String password = addPatientViewModel.getPassword();
        String name = addPatientViewModel.getName();
        String birthdate = addPatientViewModel.getBirthdate();
        String gender = addPatientViewModel.getGender();
        String address = addPatientViewModel.getAddress();

        this.patientSender.sendPatient(username,password,name,birthdate,gender,address);
    }
    }



