package com.sd.careapp.controller;

import com.sd.careapp.Mapper;
import com.sd.careapp.entity.Caregiver;
import com.sd.careapp.entity.User;
import com.sd.careapp.repository.CaregiverRepository;
import com.sd.careapp.repository.UserRepository;
import com.sd.careapp.viewmodel.CaregiverViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.ValidationException;
import java.util.List;

@RestController
@RequestMapping(value = "/doctor")
@CrossOrigin
public class DoctorController {

    private Mapper mapper;
    private UserRepository urp;


    @Autowired
    public DoctorController(UserRepository urp){
        this.urp = urp;
    }

    @RequestMapping(value = "/patients", method = RequestMethod.GET)
    public List<User> getAllPatients(){
        return  urp.findPatients();
    }

    @GetMapping(value = "/caregivers")
    public List<User> getAllCaregivers()
    {
        var allCaregivers = this.urp.findCaregivers();
        return allCaregivers;
    }

    @PostMapping
    public User saveCaregiver(@RequestBody CaregiverViewModel caregiverViewModel, BindingResult bindingResult){

            try {
                bindingResult.hasErrors();
            }
            catch (DataBindingException e) {
                e.printStackTrace();
            }

            var caregiverEntity = this.mapper.convertToCaregiverEntity(caregiverViewModel);

            this.urp.save(caregiverEntity);

            return  caregiverEntity;

    }

    DoctorController(){

    }


}
