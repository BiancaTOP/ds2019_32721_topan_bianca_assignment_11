package com.sd.careapp.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.sd.careapp.entity.Caregiver;
import com.sd.careapp.entity.User;
import com.sd.careapp.entity.Authority;
import com.sd.careapp.viewmodel.CaregiverViewModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public Optional<User> findByUsername(String username);

    /*
    @Query(value = "SELECT u.id as user_id, u.username as username , u.name as name, u.gender as gender, u.birthdate as birthdate, u.address as address FROM User u")
    Iterable<User> findAllUsers();
    */
    public static final String FIND_PATIENTS = "SELECT u.id, u.username, u.name, u.birthdate, u.gender, u.address FROM User u join authority a on u.id = a.id where a.authority='ROLE_PATIENT'";

    @Query(value = FIND_PATIENTS, nativeQuery = true)
    List<User> findPatients();

    public static final String FIND_CAREGIVERS = "SELECT u.id, u.username, u.password, u.name, u.birthdate, u.gender, u.address FROM User u join authority a on u.id = a.id where a.authority='ROLE_CAREGIVER'";
    @Query(value = FIND_CAREGIVERS, nativeQuery = true)
    List<User> findCaregivers();






}