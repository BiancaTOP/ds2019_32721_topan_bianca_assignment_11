package com.sd.careapp.repository;

import com.sd.careapp.entity.Caregiver;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CaregiverRepository {

    public static final String FIND_CAREGIVERS = "SELECT u.id, u.username, u.password, u.name, u.birthdate, u.gender, u.address FROM User u join authority a on u.id = a.id where a.authority='ROLE_CAREGIVER'";
    @Query(value = FIND_CAREGIVERS, nativeQuery = true)
    List<Caregiver> findCaregivers();

    void save(Caregiver caregiverEntity);
}
